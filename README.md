## custom C libs
small collection of custom C libraries.
run make / sudo make install to stick them in your /usr/local/lib folder!


libs included so far are:

libsplit (-lsplit): splits a given string into slices based on an array of char delimiters


if you want to run executables compiled against these libraries, make sure you have included /usr/local/lib in $LD_LIBRARY_PATH
