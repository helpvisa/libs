#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int splitstr(char *str, char ***slices, char delims[]) {
	int length = strlen(str);

	if (*slices) {
		return -1;
	}
	// worst case scenario: each word is only one letter
	*slices = malloc(length * sizeof(char *));

	int wc = 0;
	int idx = 0;
	while (idx < length) {
		char *word = malloc((length + 1) * sizeof(char));
		int l = 0;
		int valid = 1;
		while (valid && str[idx] != '\0' && idx < length) {
			int d;
			for (d = 0; d < strlen(delims); d++) {
				if (str[idx] == delims[d]) {
					valid = 0;
				}
			}

			if (valid) {
				word[l] = tolower(str[idx]);
				l++;
				idx++;
			}
		}

		// set null terminator, increment wordcount and index
		if (l > 0) {
			word[l] = '\0';
			*(*slices + wc) = word;
			wc++;
		} else {
			free(word);
		}

		idx++;
	}

	return wc;
}
